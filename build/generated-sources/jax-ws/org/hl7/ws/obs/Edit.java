
package org.hl7.ws.obs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for edit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="edit"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hL7Observation" type="{http://obs.ws.hl7.org/}hl7Observation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edit", propOrder = {
    "hl7Observation"
})
public class Edit {

    @XmlElement(name = "hL7Observation")
    protected Hl7Observation hl7Observation;

    /**
     * Gets the value of the hl7Observation property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Observation }
     *     
     */
    public Hl7Observation getHL7Observation() {
        return hl7Observation;
    }

    /**
     * Sets the value of the hl7Observation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Observation }
     *     
     */
    public void setHL7Observation(Hl7Observation value) {
        this.hl7Observation = value;
    }

}
