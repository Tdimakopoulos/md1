
package org.hl7.ws.realtime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for remove complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="remove"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hL7ObservationRealTime" type="{http://realtime.ws.hl7.org/}hl7ObservationRealTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "remove", propOrder = {
    "hl7ObservationRealTime"
})
public class Remove {

    @XmlElement(name = "hL7ObservationRealTime")
    protected Hl7ObservationRealTime hl7ObservationRealTime;

    /**
     * Gets the value of the hl7ObservationRealTime property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7ObservationRealTime }
     *     
     */
    public Hl7ObservationRealTime getHL7ObservationRealTime() {
        return hl7ObservationRealTime;
    }

    /**
     * Sets the value of the hl7ObservationRealTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7ObservationRealTime }
     *     
     */
    public void setHL7ObservationRealTime(Hl7ObservationRealTime value) {
        this.hl7ObservationRealTime = value;
    }

}
