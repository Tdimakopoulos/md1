/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.dashboard;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.xml.ws.WebServiceRef;
import org.hl7.ws.obs.Obshl7_Service;
import org.hl7.ws.realtime.RealTimeOBS_Service;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class dashboard {

    
    private RealTimeOBS_Service service;

    private Obshl7_Service service_4;

    private LineChartModel dateModel;
    private LineChartModel dateModel2;
    private LineChartModel dateModel3;
    private LineChartModel dateModel5;
    private LineChartModel dateModel6;
    private LineChartModel dateModel9;
    private LineChartModel dateModel10;
    private LineChartModel dateModel11;
    private LineChartModel dateModel15;
    private LineChartModel dateModel23;
    private LineChartModel dateModel30;

    
    private LineChartModel realtimetr;
    
    private String szstatus;
    
    private String temp1;
    private String temp2;
    private String temp3;
    private Map<String, String> values;
    String path = "/root/HL7/MD1/";
int is=0;

    public void writeFile2(String file, String line) throws IOException {
        FileWriter fw = new FileWriter(path + file);

        fw.write(line);

        fw.close();
    }

    public void buttonActionstart(ActionEvent actionEvent) {
        addMessage("Calibration Started!");
         szstatus="Ready for Calibration";
        is=1;
        try {
            writeFile2("start.ph", "start");
        } catch (IOException e) {

        }
    }

    public void buttonActionph14(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 1 - Ph 4!");
    is=2;
        try {
            writeFile2("c.ph", "14");
        } catch (IOException e) {

        }
    }

    public void buttonActionph17(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 1 - Ph 7!");
    is=3;
        try {
            writeFile2("c.ph", "17");
        } catch (IOException e) {

        }
    }

    public void buttonActionph110(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 1 - Ph 10!");
    is=4;
        try {
            writeFile2("c.ph", "110");
        } catch (IOException e) {

        }
    }

    public void buttonActionph24(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 2 - Ph 4!");
    is=5;
        try {
            writeFile2("c.ph", "24");
        } catch (IOException e) {

        }
    }

    public void buttonActionph27(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 2 - Ph 7!");
    is=6;
        try {
            writeFile2("c.ph", "27");
        } catch (IOException e) {

        }
    }

    public void LoadGraphDataxx() {
        if(is==1)
            szstatus="Ready for Calibration";
        else if(is==0)
            szstatus="Device Online";
        else if(is==8)
            szstatus="Device is Calibrated";
        else
            szstatus="Value Received!!!";
    }
    public void buttonActionph210(ActionEvent actionEvent) {
        addMessage("Calibration Started Ph Sensor 2 - Ph 10!");
    is=7;
        try {
            writeFile2("c.ph", "210");
        } catch (IOException e) {

        }
    }

    public void buttonActionend(ActionEvent actionEvent) {
        addMessage("Calibration Ended!");
    is=8;
        try {
            writeFile2("end.ph", "end");
        } catch (IOException e) {

        }
    }

    public void buttonActionTemp(ActionEvent actionEvent) {
        addMessage("Values Saved!");
        System.out.println("org.hl7.dashboard.dashboard.buttonActionTemp()");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Creates a new instance of dashboard
     */
    public dashboard() {
        LoadGraphData();
        values = new HashMap<String, String>();
        values.put("3610", "3610");
        values.put("3694", "3694");

    }

    public String Convert(int itype) {

        if (itype == 1) {
            return "MMP-9_DARK";
        }
        if (itype == 2) {
            return "MMP-9_630";
        }
        if (itype == 3) {
            return "MMP-9_850";
        }
        if (itype == 4) {
            return "MMPSENS_STATUS";
        }
        if (itype == 5) {
            return "pH1";
        }
        if (itype == 6) {
            return "pH2";
        }
        if (itype == 7) {
            return "pH1SNES_STATUS";
        }
        if (itype == 8) {
            return "pH2SENS_STATUS";
        }
        if (itype == 9) {
            return "T1";
        }
        if (itype == 10) {
            return "T2";
        }
        if (itype == 11) {
            return "T3";
        }
        if (itype == 12) {
            return "T1SENS_STATUS";
        }
        if (itype == 13) {
            return "T2SENS_STATUS";
        }
        if (itype == 14) {
            return "T3SENS_STATUS";
        }
        if (itype == 15) {
            return "CBP";
        }
        if (itype == 16) {
            return "CBP_STATUS";
        }
        if (itype == 17) {
            return "BATT_SOC";
        }
        if (itype == 18) {
            return "BATT_CAP";
        }
        if (itype == 19) {
            return "BATT_V";
        }
        if (itype == 20) {
            return "BATT_I";
        }
        if (itype == 21) {
            return "BATT_TEMP";
        }
        if (itype == 22) {
            return "BATT_STATUS";
        }
        if (itype == 23) {
            return "IWSD_ATTITUDE";
        }
        if (itype == 24) {
            return "IWSD_STATUS";
        }
        if (itype == 30) {
            return "TREATMENT";
        }
        return "UNKNOWN";
    }

    public String convertDate(Long mil) {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");

        // Get the date today using Calendar object.
        Date today = new Date(mil);
        // Using DateFormat format method we can create a string 
        // representation of a date with the defined format.
        String reportDate = df.format(today);
        return reportDate;
    }

    public void LoadGraphData() {
        System.out.println("Loading Data");
        dateModel = new LineChartModel();
        dateModel2 = new LineChartModel();
        dateModel3 = new LineChartModel();
        dateModel5 = new LineChartModel();
        dateModel6 = new LineChartModel();
        dateModel9 = new LineChartModel();
        dateModel10 = new LineChartModel();
        dateModel11 = new LineChartModel();
        dateModel15 = new LineChartModel();
        dateModel23 = new LineChartModel();
        realtimetr = new LineChartModel();
        
        LineChartSeries tr = new LineChartSeries();
        tr.setLabel("Treatment");
        String lastdtr=null;
        
        LineChartSeries series30 = new LineChartSeries();
        series30.setLabel("Series 30");
        String lastdate30 = null;

        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("Series 2");
        String lastdate2 = null;

        LineChartSeries series3 = new LineChartSeries();
        series3.setLabel("Series 3");
        String lastdate3 = null;

        LineChartSeries series5 = new LineChartSeries();
        series5.setLabel("Series 5");
        String lastdate5 = null;

        LineChartSeries series6 = new LineChartSeries();
        series6.setLabel("Series 6");
        String lastdate6 = null;

        LineChartSeries series9 = new LineChartSeries();
        series9.setLabel("Series 9");
        String lastdate9 = null;

        LineChartSeries series10 = new LineChartSeries();
        series10.setLabel("Series 10");
        String lastdate10 = null;

        LineChartSeries series11 = new LineChartSeries();
        series11.setLabel("Series 11");
        String lastdate11 = null;

        LineChartSeries series15 = new LineChartSeries();
        series15.setLabel("Series 15");
        String lastdate15 = null;

        LineChartSeries series23 = new LineChartSeries();
        series23.setLabel("Series 23");
        String lastdate23 = null;

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Series 1");
        String lastdate = null;
        
        Long selpatL = new Long(2);
        
        java.util.List<org.hl7.ws.realtime.Hl7ObservationRealTime> px=findAll_1();
        
        for (int idxx = 0; idxx < px.size(); idxx++) {
            Date CheckDate = new Date();
            CheckDate.setTime(px.get(idxx).getLngdate());
            if ((px.get(idxx).getLpatient().equals(selpatL))) //                    && (DateUtils.isSameDay(getChartDate(), CheckDate))) 
            {
                tr.set(convertDate(px.get(idxx).getLngdate()), px.get(idxx).getIntvalue());
                    lastdtr = convertDate(px.get(idxx).getLngdate());
            }
        }
        
        realtimetr.addSeries(tr);
        realtimetr.getAxis(AxisType.Y).setLabel("Treatment");
        DateAxis axisttr = new DateAxis("Dates");
        axisttr.setTickAngle(-50);
        axisttr.setMax(lastdtr);
        axisttr.setTickFormat("%H:%#M:%S");
        realtimetr.getAxes().put(AxisType.X, axisttr);
        
        java.util.List<org.hl7.ws.obs.Hl7Observation> pobs = findAll();
        for (int i = 0; i < pobs.size(); i++) {
            Date CheckDate = new Date();
            CheckDate.setTime(pobs.get(i).getLngdate());
            if ((pobs.get(i).getLpatient().equals(selpatL))) //                    && (DateUtils.isSameDay(getChartDate(), CheckDate))) 
            {

                if (pobs.get(i).getName().equalsIgnoreCase("1")) {
                    series1.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("2")) {
                    series2.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate2 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("30")) {
                    series30.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate30 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("3")) {
                    series3.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate3 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("5")) {
                    series5.set(convertDate(pobs.get(i).getLngdate()), Double.valueOf(pobs.get(i).getStrvalue()));
                    lastdate5 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("6")) {
                    series6.set(convertDate(pobs.get(i).getLngdate()), Double.valueOf(pobs.get(i).getStrvalue()));
                    lastdate6 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("9")) {
                    series9.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate9 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("10")) {
                    series10.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate10 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("11")) {
                    series11.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate11 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("15")) {
                    series15.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate15 = convertDate(pobs.get(i).getLngdate());

                }

                if (pobs.get(i).getName().equalsIgnoreCase("23")) {
                    series23.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                    lastdate23 = convertDate(pobs.get(i).getLngdate());

                }

            }//end of if check
        }

//        dateModel = new LineChartModel();
//        LineChartSeries series1 = new LineChartSeries();
//        series1.setLabel("Series 1");
//        series1.set("2014-01-01 00:10:50", 51);
//        series1.set("2014-01-01 00:10:51", 22);
//        series1.set("2014-01-01 00:10:52", 65);
//        series1.set("2014-01-01 00:10:53", 74);
//        series1.set("2014-01-01 00:10:55", 24);
//        series1.set("2014-01-01 00:10:56", 51);
        dateModel.addSeries(series1);
        dateModel.getAxis(AxisType.Y).setLabel(Convert(1));
        DateAxis axis = new DateAxis("Dates");
        axis.setTickAngle(-50);
        axis.setMax(lastdate);
        axis.setTickFormat("%H:%#M:%S");
        dateModel.getAxes().put(AxisType.X, axis);

        dateModel2.addSeries(series2);
        dateModel2.getAxis(AxisType.Y).setLabel(Convert(2));
        DateAxis axis2 = new DateAxis("Dates");
        axis2.setTickAngle(-50);
        axis2.setMax(lastdate2);
        axis2.setTickFormat("%H:%#M:%S");
        dateModel2.getAxes().put(AxisType.X, axis2);

//        getDateModel30().addSeries(series30);
//        getDateModel30().getAxis(AxisType.Y).setLabel(Convert(2));
//        DateAxis axis30 = new DateAxis("Dates");
//        axis30.setTickAngle(-50);
//        axis30.setMax(lastdate30);
//        axis30.setTickFormat("%H:%#M:%S");
//        getDateModel30().getAxes().put(AxisType.X, axis30);
        dateModel3.addSeries(series3);
        dateModel3.getAxis(AxisType.Y).setLabel(Convert(3));
        DateAxis axis3 = new DateAxis("Dates");
        axis3.setTickAngle(-50);
        axis3.setMax(lastdate3);
        axis3.setTickFormat("%H:%#M:%S");
        dateModel3.getAxes().put(AxisType.X, axis3);

        dateModel5.addSeries(series5);
        dateModel5.getAxis(AxisType.Y).setLabel(Convert(5));
        DateAxis axis5 = new DateAxis("Dates");
        axis5.setTickAngle(-50);
        axis5.setMax(lastdate5);
        axis5.setTickFormat("%H:%#M:%S");
        dateModel5.getAxes().put(AxisType.X, axis5);

        dateModel6.addSeries(series6);
        dateModel6.getAxis(AxisType.Y).setLabel(Convert(6));
        DateAxis axis6 = new DateAxis("Dates");
        axis6.setTickAngle(-50);
        axis6.setMax(lastdate6);
        axis6.setTickFormat("%H:%#M:%S");
        dateModel6.getAxes().put(AxisType.X, axis6);

        dateModel9.addSeries(series9);
        dateModel9.getAxis(AxisType.Y).setLabel(Convert(9));
        DateAxis axis9 = new DateAxis("Dates");
        axis9.setTickAngle(-50);
        axis9.setMax(lastdate9);
        axis9.setTickFormat("%H:%#M:%S");
        dateModel9.getAxes().put(AxisType.X, axis9);

        dateModel10.addSeries(series10);
        dateModel10.getAxis(AxisType.Y).setLabel(Convert(10));
        DateAxis axis10 = new DateAxis("Dates");
        axis10.setTickAngle(-50);
        axis10.setMax(lastdate10);
        axis10.setTickFormat("%H:%#M:%S");
        dateModel10.getAxes().put(AxisType.X, axis10);

        dateModel11.addSeries(series11);
        dateModel11.getAxis(AxisType.Y).setLabel(Convert(11));
        DateAxis axis11 = new DateAxis("Dates");
        axis11.setTickAngle(-50);
        axis11.setMax(lastdate11);
        axis11.setTickFormat("%H:%#M:%S");
        dateModel11.getAxes().put(AxisType.X, axis11);

        dateModel15.addSeries(series15);
        dateModel15.getAxis(AxisType.Y).setLabel(Convert(15));
        DateAxis axis15 = new DateAxis("Dates");
        axis15.setTickAngle(-50);
        axis15.setMax(lastdate15);
        axis15.setTickFormat("%H:%#M:%S");
        dateModel15.getAxes().put(AxisType.X, axis15);

        dateModel23.addSeries(series23);
        dateModel23.getAxis(AxisType.Y).setLabel(Convert(23));
        DateAxis axis23 = new DateAxis("Dates");
        axis23.setTickAngle(-50);
        axis23.setMax(lastdate23);
        axis23.setTickFormat("%H:%#M:%S");
        dateModel23.getAxes().put(AxisType.X, axis23);

    }

    private java.util.List<org.hl7.ws.obs.Hl7Observation> findAll() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        Obshl7_Service service_4 = new Obshl7_Service();
        org.hl7.ws.obs.Obshl7 port = service_4.getObshl7Port();
        return port.findAll();
    }

    /**
     * @return the service_4
     */
    public Obshl7_Service getService_4() {
        return service_4;
    }

    /**
     * @param service_4 the service_4 to set
     */
    public void setService_4(Obshl7_Service service_4) {
        this.service_4 = service_4;
    }

    /**
     * @return the dateModel
     */
    public LineChartModel getDateModel() {
        return dateModel;
    }

    /**
     * @param dateModel the dateModel to set
     */
    public void setDateModel(LineChartModel dateModel) {
        this.dateModel = dateModel;
    }

    /**
     * @return the dateModel2
     */
    public LineChartModel getDateModel2() {
        return dateModel2;
    }

    /**
     * @param dateModel2 the dateModel2 to set
     */
    public void setDateModel2(LineChartModel dateModel2) {
        this.dateModel2 = dateModel2;
    }

    /**
     * @return the dateModel3
     */
    public LineChartModel getDateModel3() {
        return dateModel3;
    }

    /**
     * @param dateModel3 the dateModel3 to set
     */
    public void setDateModel3(LineChartModel dateModel3) {
        this.dateModel3 = dateModel3;
    }

    /**
     * @return the dateModel5
     */
    public LineChartModel getDateModel5() {
        return dateModel5;
    }

    /**
     * @param dateModel5 the dateModel5 to set
     */
    public void setDateModel5(LineChartModel dateModel5) {
        this.dateModel5 = dateModel5;
    }

    /**
     * @return the dateModel6
     */
    public LineChartModel getDateModel6() {
        return dateModel6;
    }

    /**
     * @param dateModel6 the dateModel6 to set
     */
    public void setDateModel6(LineChartModel dateModel6) {
        this.dateModel6 = dateModel6;
    }

    /**
     * @return the dateModel9
     */
    public LineChartModel getDateModel9() {
        return dateModel9;
    }

    /**
     * @param dateModel9 the dateModel9 to set
     */
    public void setDateModel9(LineChartModel dateModel9) {
        this.dateModel9 = dateModel9;
    }

    /**
     * @return the dateModel10
     */
    public LineChartModel getDateModel10() {
        return dateModel10;
    }

    /**
     * @param dateModel10 the dateModel10 to set
     */
    public void setDateModel10(LineChartModel dateModel10) {
        this.dateModel10 = dateModel10;
    }

    /**
     * @return the dateModel11
     */
    public LineChartModel getDateModel11() {
        return dateModel11;
    }

    /**
     * @param dateModel11 the dateModel11 to set
     */
    public void setDateModel11(LineChartModel dateModel11) {
        this.dateModel11 = dateModel11;
    }

    /**
     * @return the dateModel15
     */
    public LineChartModel getDateModel15() {
        return dateModel15;
    }

    /**
     * @param dateModel15 the dateModel15 to set
     */
    public void setDateModel15(LineChartModel dateModel15) {
        this.dateModel15 = dateModel15;
    }

    /**
     * @return the dateModel23
     */
    public LineChartModel getDateModel23() {
        return dateModel23;
    }

    /**
     * @param dateModel23 the dateModel23 to set
     */
    public void setDateModel23(LineChartModel dateModel23) {
        this.dateModel23 = dateModel23;
    }

    /**
     * @return the dateModel30
     */
    public LineChartModel getDateModel30() {
        return dateModel30;
    }

    /**
     * @param dateModel30 the dateModel30 to set
     */
    public void setDateModel30(LineChartModel dateModel30) {
        this.dateModel30 = dateModel30;
    }

    /**
     * @return the temp1
     */
    public String getTemp1() {
        return temp1;
    }

    /**
     * @param temp1 the temp1 to set
     */
    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    /**
     * @return the temp2
     */
    public String getTemp2() {
        return temp2;
    }

    /**
     * @param temp2 the temp2 to set
     */
    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    /**
     * @return the values
     */
    public Map<String, String> getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(Map<String, String> values) {
        this.values = values;
    }

    /**
     * @return the temp3
     */
    public String getTemp3() {
        return temp3;
    }

    /**
     * @param temp3 the temp3 to set
     */
    public void setTemp3(String temp3) {
        this.temp3 = temp3;
    }

    /**
     * @return the realtimetr
     */
    public LineChartModel getRealtimetr() {
        return realtimetr;
    }

    /**
     * @param realtimetr the realtimetr to set
     */
    public void setRealtimetr(LineChartModel realtimetr) {
        this.realtimetr = realtimetr;
    }

    private java.util.List<org.hl7.ws.realtime.Hl7ObservationRealTime> findAll_1() {
        RealTimeOBS_Service service = new RealTimeOBS_Service(); 
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.hl7.ws.realtime.RealTimeOBS port = service.getRealTimeOBSPort();
        return port.findAll();
    }

    /**
     * @return the szstatus
     */
    public String getSzstatus() {
        return szstatus;
    }

    /**
     * @param szstatus the szstatus to set
     */
    public void setSzstatus(String szstatus) {
        this.szstatus = szstatus;
    }
}
